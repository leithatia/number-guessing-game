package com.academiadecodigo.numberguessinggame;

public class GuessingGame {
    public static void main(String[] args) {
        GameMaster gm = new GameMaster();
        Player player = new Player();

        gm.setMasterNum();
        player.setPlayerNum();

        boolean gameover = gm.isMatch(player);

        while (!gameover) {
            player.setPlayerNum(); // player picks a number
            gameover = gm.isMatch(player); // check if number is the correct number and if so gameover
            if(!gameover) {
                System.out.println("sorry " + player.getPlayerNum() + " is not the number");
            }
        }

        System.out.println("you got it " + player.getPlayerNum());
    }
}
