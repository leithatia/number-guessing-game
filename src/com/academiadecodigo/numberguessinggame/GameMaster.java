package com.academiadecodigo.numberguessinggame;

public class GameMaster {
    private int masterNum;

    public void setMasterNum() {
        masterNum = Generator.randomNumber();
    }

    public boolean isMatch(Player player) {
        return player.getPlayerNum() == masterNum;
    }

}
