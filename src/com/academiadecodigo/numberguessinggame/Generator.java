package com.academiadecodigo.numberguessinggame;

public class Generator {
    // return a random number from 1 to 10
    public static int randomNumber() {
        return (int)Math.ceil(Math.random() * 10);
    }
}
