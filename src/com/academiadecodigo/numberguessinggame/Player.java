package com.academiadecodigo.numberguessinggame;

public class Player {

    private int playerNum;

    public void setPlayerNum() {
        playerNum = Generator.randomNumber();
    }

    public int getPlayerNum(){
        return playerNum;
    }
}
